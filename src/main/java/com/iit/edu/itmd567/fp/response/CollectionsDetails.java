/**
 * 
 */
package com.iit.edu.itmd567.fp.response;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author Admin
 *
 */
public class CollectionsDetails {

	@JsonProperty("collections")
	private List<CollectionRequest> collections;
	@JsonProperty("has_more")
	private String has_more;
	@JsonProperty("entity_id")
	private Integer entity_id;
	@JsonProperty("has_total")
	private String has_total;
	@JsonProperty("display_text")
	private String display_text;
	@JsonProperty("share_url")
	private String share_url;

	public CollectionsDetails() {
	}

	public CollectionsDetails(List<CollectionRequest> collections, String has_more, Integer entity_id, String has_total,
			String display_text, String share_url) {
		super();
		this.collections = collections;
		this.has_more = has_more;
		this.entity_id = entity_id;
		this.has_total = has_total;
		this.display_text = display_text;
		this.share_url = share_url;
	}

	public List<CollectionRequest> getCollections() {
		return collections;
	}

	public void setCollections(List<CollectionRequest> collections) {
		this.collections = collections;
	}

	public String getHas_more() {
		return has_more;
	}

	public void setHas_more(String has_more) {
		this.has_more = has_more;
	}

	public String getHas_total() {
		return has_total;
	}

	public void setHas_total(String has_total) {
		this.has_total = has_total;
	}

	public String getDisplay_text() {
		return display_text;
	}

	public void setDisplay_text(String display_text) {
		this.display_text = display_text;
	}

	public String getShare_url() {
		return share_url;
	}

	public void setShare_url(String share_url) {
		this.share_url = share_url;
	}

	public Integer getEntity_id() {
		return entity_id;
	}

	public void setEntity_id(Integer entity_id) {
		this.entity_id = entity_id;
	}

	@Override
	public String toString() {
		return "CollectionsDetails [collections=" + collections + ", has_more=" + has_more + ", entity_id=" + entity_id
				+ ", has_total=" + has_total + ", display_text=" + display_text + ", share_url=" + share_url + "]";
	}

}
