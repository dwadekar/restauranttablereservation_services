/**
 * 
 */
package com.iit.edu.itmd567.fp.response;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author Admin
 *
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class UserRating {

	@JsonProperty("aggregate_rating")
	private String aggregateRating;
	@JsonProperty("rating_text")
	private String ratingText;
	@JsonProperty("rating_color")
	private String ratingColor;
	@JsonProperty("votes")
	private String votes;

	public UserRating() {
	}

	public UserRating(String aggregateRating, String ratingText, String ratingColor, String votes) {
		super();
		this.aggregateRating = aggregateRating;
		this.ratingText = ratingText;
		this.ratingColor = ratingColor;
		this.votes = votes;
	}

	public String getAggregateRating() {
		return aggregateRating;
	}

	public void setAggregateRating(String aggregateRating) {
		this.aggregateRating = aggregateRating;
	}

	public String getRatingText() {
		return ratingText;
	}

	public void setRatingText(String ratingText) {
		this.ratingText = ratingText;
	}

	public String getRatingColor() {
		return ratingColor;
	}

	public void setRatingColor(String ratingColor) {
		this.ratingColor = ratingColor;
	}

	public String getVotes() {
		return votes;
	}

	public void setVotes(String votes) {
		this.votes = votes;
	}

	@Override
	public String toString() {
		return "UserRating [aggregateRating=" + aggregateRating + ", ratingText=" + ratingText + ", ratingColor="
				+ ratingColor + ", votes=" + votes + "]";
	}

}
