/**
 * 
 */
package com.iit.edu.itmd567.fp.service;

import java.util.Collection;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.iit.edu.itmd567.fp.model.Customer;
import com.iit.edu.itmd567.fp.repository.CustomerRepository;

/**
 * @author Admin
 *
 */
@Component
public class CustomerService {

	@Autowired
	private CustomerRepository customerRepository;

	@Bean
	public PasswordEncoder getPasswordEncoder() {
		return new BCryptPasswordEncoder();
	}

	@Transactional
	public void add(Customer customer) {
		customer.setPassword(getPasswordEncoder().encode(customer.getPassword()));
		customerRepository.save(customer);
	}

	@Transactional
	public void update(Customer customer) {
		customerRepository.save(customer);
	}

	@Transactional(readOnly = true)
	public List<Customer> findAll() {
		return customerRepository.findAll();
	}

	@Transactional
	public void addAll(Collection<Customer> customers) {
		for (Customer customer : customers) {
			customerRepository.save(customer);
		}
	}

	@Transactional(readOnly = true)
	public List<Customer> findByCustomerID(String customerId) {
		return customerRepository.findByCustomerId(customerId);
	}

	@Transactional(readOnly = true)
	public List<Customer> findByNameContainingIgnoreCase(String lastName) {
		return customerRepository.findByLastNameIs(lastName);
	}

	@Transactional(readOnly = true)
	public Customer getCustomerByUsername(String userName) {
		return customerRepository.getCustomerByUsername(userName);
	}

	@Transactional
	public void delete(List<Customer> customers) {
		customerRepository.deleteInBatch(customers);
	}
}
