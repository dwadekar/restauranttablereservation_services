/**
 * 
 */
package com.iit.edu.itmd567.fp.response;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author Admin
 *
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class UserReviews {

	@JsonProperty("review")
	private Review review;

	public UserReviews() {
	}

	public UserReviews(Review review) {
		super();
		this.review = review;
	}

	public Review getReview() {
		return review;
	}

	public void setReview(Review review) {
		this.review = review;
	}

	@Override
	public String toString() {
		return "UserReviews [review=" + review + "]";
	}

}
