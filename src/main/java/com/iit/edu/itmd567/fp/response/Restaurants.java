/**
 * 
 */
package com.iit.edu.itmd567.fp.response;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author Admin
 *
 */
public class Restaurants {

	@JsonProperty("restaurant")
	private Restaurant restaurant;

	public Restaurants() {
		super();
	}

	public Restaurants(Restaurant restaurant) {
		super();
		this.restaurant = restaurant;
	}

	public Restaurant getRestaurant() {
		return restaurant;
	}

	public void setRestaurant(Restaurant restaurant) {
		this.restaurant = restaurant;
	}

	@Override
	public String toString() {
		return "Restaurants [restaurant=" + restaurant + "]";
	}

}
