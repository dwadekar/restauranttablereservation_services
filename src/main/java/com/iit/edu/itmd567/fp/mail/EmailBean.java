/**
 * 
 */
package com.iit.edu.itmd567.fp.mail;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.MailSender;
import org.springframework.stereotype.Component;

/**
 * @author Admin
 *
 */
@Component
public class EmailBean {

	private static final Logger logger = Logger.getLogger(EmailBean.class);
	
	// MailSender interface defines a strategy for sending simple mails
	@Autowired
	private MailSender mailSender;
	
	public void sendMail(String to, String subject, String body) {
		logger.info("Email bean called with parameters as: " + to + ", " + subject + ", " + body);
		
		// creates a simple e-mail object
        SimpleMailMessage email = new SimpleMailMessage();
        email.setTo(to);
        email.setSubject(subject);
        email.setText(body);
         
        // sends the e-mail
        mailSender.send(email);
	}
}
