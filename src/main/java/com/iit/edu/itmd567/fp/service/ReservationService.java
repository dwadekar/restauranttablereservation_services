/**
 * 
 */
package com.iit.edu.itmd567.fp.service;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.iit.edu.itmd567.fp.enums.ReservationStatus;
import com.iit.edu.itmd567.fp.model.Reservation;
import com.iit.edu.itmd567.fp.repository.ReservationRepository;

/**
 * @author Admin
 *
 */
@Component
public class ReservationService {

	private static final Logger logger = Logger.getLogger(ReservationService.class);

	@Autowired
	private ReservationRepository reservationRepository;

	@Transactional
	public void add(Reservation reservation) {
		reservationRepository.save(reservation);
	}

	@Transactional(readOnly = true)
	public List<Reservation> findByRestaurantID(String restaurantId) {
		logger.info("About to search by Restaurant ID:" + restaurantId);
		return reservationRepository.findByRestaurantID(restaurantId);
	}

	@Transactional(readOnly = true)
	public Reservation findByReservationId(String reservationId) {
		logger.info("About to search by Reservation ID:" + reservationId);
		return reservationRepository.findByReservationId(reservationId);
	}

	@Transactional(readOnly = true)
	public List<Reservation> findByUserName(String userName) {
		logger.info("About to search by Username:" + userName);
		return reservationRepository.findByUserName(userName);
	}

	@Transactional(readOnly = true)
	public List<Reservation> findAll() {
		return reservationRepository.findAll();
	}

	@Transactional
	public Reservation cancelReservation(String reservationId) {
		Reservation reservation = reservationRepository.findByReservationId(reservationId);

		reservation.setStatus(ReservationStatus.CANCEL.getType());
		reservationRepository.save(reservation);

		reservation = reservationRepository.findByReservationId(reservationId);
		return reservation;
	}

	@Transactional
	public void delete(List<Reservation> reservations) {
		reservationRepository.deleteInBatch(reservations);
	}

}
