/**
 * 
 */
package com.iit.edu.itmd567.fp.enums;

/**
 * @author Admin
 *
 */
public enum ReservationStatus {
	ACTIVE("Active"), CANCEL("Cancelled");
	
	private final String type;

	private ReservationStatus(String type) {
		this.type = type;
	}

	public String getType() {
		return type;
	}
}
