/**
 * 
 */
package com.iit.edu.itmd567.fp.response;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author Admin
 *
 */
public class LocationsRequest {

	@JsonProperty("location_suggestions")
	private List<LocationDetails> location_suggestions;
	@JsonProperty("status")
	private String status;
	@JsonIgnore
	private Integer has_more;
	@JsonIgnore
	private Integer has_total;

	public LocationsRequest() {
	}

	public LocationsRequest(List<LocationDetails> location_suggestions, String status, Integer has_more,
			Integer has_total) {
		this.location_suggestions = location_suggestions;
		this.status = status;
		this.has_more = has_more;
		this.has_total = has_total;
	}

	public List<LocationDetails> getLocation_suggestions() {
		return location_suggestions;
	}

	public void setLocation_suggestions(List<LocationDetails> location_suggestions) {
		this.location_suggestions = location_suggestions;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Integer getHas_more() {
		return has_more;
	}

	public void setHas_more(Integer has_more) {
		this.has_more = has_more;
	}

	public Integer getHas_total() {
		return has_total;
	}

	public void setHas_total(Integer has_total) {
		this.has_total = has_total;
	}

	@Override
	public String toString() {
		return "LocationsRequest [location_suggestions=" + location_suggestions + ", status=" + status + ", has_more="
				+ has_more + ", has_total=" + has_total + "]";
	}

}
