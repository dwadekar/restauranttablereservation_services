/**
 * 
 */
package com.iit.edu.itmd567.fp.response;

import java.math.BigInteger;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author Admin
 *
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class Review {

	@JsonProperty("rating")
	private Double rating;
	@JsonProperty("review_text")
	private String reviewText;
	@JsonProperty("id")
	private String id;
	@JsonProperty("rating_color")
	private String ratingColor;
	@JsonProperty("review_time_friendly")
	private String reviewTimeFriendly;
	@JsonProperty("rating_text")
	private String ratingText;
	@JsonProperty("timestamp")
	private BigInteger timestamp;
	@JsonProperty("user")
	private User user;

	public Review() {
	}

	public Review(Double rating, String reviewText, String id, String ratingColor, String reviewTimeFriendly,
			String ratingText, BigInteger timestamp, User user) {
		super();
		this.rating = rating;
		this.reviewText = reviewText;
		this.id = id;
		this.ratingColor = ratingColor;
		this.reviewTimeFriendly = reviewTimeFriendly;
		this.ratingText = ratingText;
		this.timestamp = timestamp;
		this.user = user;
	}

	public Double getRating() {
		return rating;
	}

	public void setRating(Double rating) {
		this.rating = rating;
	}

	public String getReviewText() {
		return reviewText;
	}

	public void setReviewText(String reviewText) {
		this.reviewText = reviewText;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getRatingColor() {
		return ratingColor;
	}

	public void setRatingColor(String ratingColor) {
		this.ratingColor = ratingColor;
	}

	public String getReviewTimeFriendly() {
		return reviewTimeFriendly;
	}

	public void setReviewTimeFriendly(String reviewTimeFriendly) {
		this.reviewTimeFriendly = reviewTimeFriendly;
	}

	public String getRatingText() {
		return ratingText;
	}

	public void setRatingText(String ratingText) {
		this.ratingText = ratingText;
	}

	public BigInteger getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(BigInteger timestamp) {
		this.timestamp = timestamp;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	@Override
	public String toString() {
		return "Review [rating=" + rating + ", reviewText=" + reviewText + ", id=" + id + ", ratingColor=" + ratingColor
				+ ", reviewTimeFriendly=" + reviewTimeFriendly + ", ratingText=" + ratingText + ", timestamp="
				+ timestamp + ", user=" + user + "]";
	}

}
