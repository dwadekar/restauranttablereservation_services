/**
 * 
 */
package com.iit.edu.itmd567.fp.controller;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.iit.edu.itmd567.fp.model.Customer;
import com.iit.edu.itmd567.fp.model.CustomerData;
import com.iit.edu.itmd567.fp.service.CustomerService;

/**
 * @author Admin
 *
 */
@RestController
@RequestMapping("/customers")
public class CustomerController {
	private static final Logger logger = Logger.getLogger(CustomerController.class);

	@Autowired
	private CustomerService customerService;

	@Autowired
	private CustomerData customerData;

	@GetMapping("/getCustomers")
	public List<Customer> getCustomers() {
		logger.info("Customer Info: " + customerService.findAll());

		customerData.setCustomers(customerService.findAll());
		return customerData.getCustomers();
	}

	@GetMapping("/{lastName}")
	public List<Customer> getCustomerByLastName(@PathVariable("lastName") String lastName) {
		logger.info("Customer Info: " + customerService.findByNameContainingIgnoreCase(lastName));
		return customerService.findByNameContainingIgnoreCase(lastName);
	}
	
	
}
