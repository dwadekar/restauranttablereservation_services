/**
 * 
 */
package com.iit.edu.itmd567.fp.response;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author Admin
 *
 */
public class Collection {

	@JsonProperty("collection_id")
	private Integer collection_id;
	@JsonProperty("res_count")
	private Integer res_count;
	@JsonProperty("image_url")
	private String image_url;
	@JsonProperty("url")
	private String url;
	@JsonProperty("title")
	private String title;
	@JsonProperty("description")
	private String description;
	@JsonProperty("share_url")
	private String share_url;

	public Collection() {
	}

	public Collection(Integer collection_id, Integer res_count, String image_url, String url, String title,
			String description, String share_url) {
		super();
		this.collection_id = collection_id;
		this.res_count = res_count;
		this.image_url = image_url;
		this.url = url;
		this.title = title;
		this.description = description;
		this.share_url = share_url;
	}

	public Integer getCollection_id() {
		return collection_id;
	}

	public void setCollection_id(Integer collection_id) {
		this.collection_id = collection_id;
	}

	public Integer getRes_count() {
		return res_count;
	}

	public void setRes_count(Integer res_count) {
		this.res_count = res_count;
	}

	public String getImage_url() {
		return image_url;
	}

	public void setImage_url(String image_url) {
		this.image_url = image_url;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getShare_url() {
		return share_url;
	}

	public void setShare_url(String share_url) {
		this.share_url = share_url;
	}

	@Override
	public String toString() {
		return "Collection [collection_id=" + collection_id + ", res_count=" + res_count + ", image_url=" + image_url
				+ ", url=" + url + ", title=" + title + ", description=" + description + ", share_url=" + share_url
				+ "]";
	}

}
