/**
 * 
 */
package com.iit.edu.itmd567.fp.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.MappedSuperclass;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * @author Admin
 *
 */
@MappedSuperclass
public class CommonEntity implements Serializable {

	private static final long serialVersionUID = -6178659625874707540L;

	/**
	 * Used for assign date of creation
	 */
	@JsonFormat(pattern = "yyyy-MM-dd")
	@Temporal(value = TemporalType.TIMESTAMP)
	protected Date creationDate;

	/**
	 * Default Constructor
	 */
	public CommonEntity() {
	}

	/**
	 * Used for assign date of creation
	 */
	@PrePersist
	@PreUpdate
	protected void doCreationDate() {
		this.creationDate = new Date();
	}

	/**
	 * Returns the date of creation of account for the given object
	 *
	 * @return
	 */
	public Date getCreationDate() {
		return creationDate;
	}
}
