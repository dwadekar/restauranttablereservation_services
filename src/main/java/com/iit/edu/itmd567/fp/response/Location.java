/**
 * 
 */
package com.iit.edu.itmd567.fp.response;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author Admin
 *
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class Location {

	@JsonProperty("address")
	private String address;
	@JsonProperty("locality")
	private String locality;
	@JsonProperty("city")
	private String city;
	@JsonProperty("city_id")
	private Integer cityID;
	@JsonProperty("latitude")
	private String latitude;
	@JsonProperty("longitude")
	private String longitude;
	@JsonProperty("zipcode")
	private String zipcode;
	@JsonProperty("country_id")
	private Integer countryID;
	@JsonProperty("locality_verbose")
	private String localityVerbose;

	public Location() {
	}

	public Location(String address, String locality, String city, Integer cityID, String latitude, String longitude,
			String zipcode, Integer countryID, String localityVerbose) {
		super();
		this.address = address;
		this.locality = locality;
		this.city = city;
		this.cityID = cityID;
		this.latitude = latitude;
		this.longitude = longitude;
		this.zipcode = zipcode;
		this.countryID = countryID;
		this.localityVerbose = localityVerbose;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getLocality() {
		return locality;
	}

	public void setLocality(String locality) {
		this.locality = locality;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public Integer getCityID() {
		return cityID;
	}

	public void setCityID(Integer cityID) {
		this.cityID = cityID;
	}

	public String getLatitude() {
		return latitude;
	}

	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}

	public String getLongitude() {
		return longitude;
	}

	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}

	public String getZipcode() {
		return zipcode;
	}

	public void setZipcode(String zipcode) {
		this.zipcode = zipcode;
	}

	public Integer getCountryID() {
		return countryID;
	}

	public void setCountryID(Integer countryID) {
		this.countryID = countryID;
	}

	public String getLocalityVerbose() {
		return localityVerbose;
	}

	public void setLocalityVerbose(String localityVerbose) {
		this.localityVerbose = localityVerbose;
	}

	@Override
	public String toString() {
		return "Location [address=" + address + ", locality=" + locality + ", city=" + city + ", cityID=" + cityID
				+ ", latitude=" + latitude + ", longitude=" + longitude + ", zipcode=" + zipcode + ", countryID="
				+ countryID + ", localityVerbose=" + localityVerbose + "]";
	}

}
