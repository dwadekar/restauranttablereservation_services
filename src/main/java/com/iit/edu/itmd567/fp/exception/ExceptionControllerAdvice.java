/**
 * 
 */
package com.iit.edu.itmd567.fp.exception;

import java.sql.SQLException;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

/**
 * @author Admin
 *
 */
@ControllerAdvice
public class ExceptionControllerAdvice {

	private static final Logger logger = Logger.getLogger(ExceptionControllerAdvice.class);

	@ExceptionHandler(CustomerException.class)
	public ResponseEntity<ErrorResponse> customerExceptionHandler(Exception ex) {
		ErrorResponse error = new ErrorResponse();
		error.setErrorCode(HttpStatus.PRECONDITION_FAILED.value());
		error.setMessage(ex.getMessage());
		logger.error("Customer Exception: " + ex.getMessage());
		return new ResponseEntity<ErrorResponse>(error, HttpStatus.OK);
	}

	/*@ExceptionHandler(Exception.class)
	public ResponseEntity<ErrorResponse> exceptionHandler(Exception ex) {
		logger.error("Exception occurred");
		ErrorResponse error = new ErrorResponse();
		error.setErrorCode(HttpStatus.INTERNAL_SERVER_ERROR.value());
		error.setMessage(ex.getMessage());
		return new ResponseEntity<ErrorResponse>(error, HttpStatus.OK);
	}*/

	@ExceptionHandler(SQLException.class)
	public ResponseEntity<ErrorResponse> handleSQLException(HttpServletRequest request, Exception ex) {
		logger.error("SQLException occurred");
		ErrorResponse error = new ErrorResponse();
		error.setErrorCode(HttpStatus.INTERNAL_SERVER_ERROR.value());
		error.setMessage(ex.getMessage());
		return new ResponseEntity<ErrorResponse>(error, HttpStatus.OK);
	}
}
