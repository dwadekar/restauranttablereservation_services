/**
 * 
 */
package com.iit.edu.itmd567.fp.service.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.GregorianCalendar;
import java.util.List;

import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.iit.edu.itmd567.fp.enums.AddressType;
import com.iit.edu.itmd567.fp.enums.Titles;
import com.iit.edu.itmd567.fp.enums.UserType;
import com.iit.edu.itmd567.fp.generator.CustomerIdGenerator;
import com.iit.edu.itmd567.fp.model.Address;
import com.iit.edu.itmd567.fp.model.Contact;
import com.iit.edu.itmd567.fp.model.Customer;
import com.iit.edu.itmd567.fp.service.CustomerService;

/**
 * @author Admin
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:com/iit/edu/itmd567/fp/config/app-context.xml")
public class CustomerServiceTest {

	private static final Logger logger = Logger.getLogger(CustomerServiceTest.class);

	@Autowired
	private CustomerService customerService;

	@Autowired
	private CustomerIdGenerator customeIdGenerator;

	@Before
	public void setUp() {

		Customer cust = new Customer(Titles.MRS.getTitle(), "Akriti", "G.", "Tandon", customeIdGenerator.generate(),
				new GregorianCalendar(1978, 4, 8).getTime(), "Akriti", "ctwo", UserType.CUSTOMER.getType());
		Address address = new Address("Washington", "New York", "United States", AddressType.DELIVERY.getType(),
				614283);
		Contact contact = new Contact("abc132@gmail.com", "1-320-1212458");

		cust.setAddress(address);
		address.setCustomer(cust);

		cust.setContact(contact);
		contact.setCustomer(cust);

		customerService.add(cust);
	}

	@After
	public void tearDown() {
		List<Customer> customers = customerService.findAll();
		customerService.delete(customers);
	}

	/**
	 * Test Method to test the initialization of Persistence in project
	 */
	@Test
	public void testInitPU() {
		assertTrue(true);
	}

	@Test
	public void addCustomerTest() {

		Customer customer = new Customer(Titles.MISS.getTitle(), "Remanda", "D.", "Samuel",
				customeIdGenerator.generate(), new GregorianCalendar(1985, 8, 14).getTime(), "Remanda", "cone",
				UserType.CUSTOMER.getType());
		Address address = new Address("Chicago", "Illinois", "United States", AddressType.DELIVERY.getType(), 615203);
		Contact contact = new Contact("xyz2@gmail.com", "1-800-123458");

		customer.setAddress(address);
		address.setCustomer(customer);

		customer.setContact(contact);
		contact.setCustomer(customer);

		customerService.add(customer);

		assertNotNull(customer.getId());
		assertNotNull(customer.getCustomerId());
		assertNotNull(address.getId());
		assertNotNull(contact.getId());
	}

	@Test
	public void findAllCustomerTest() {
		List<Customer> customers = customerService.findAll();

		for (Customer customer : customers) {
			assertEquals(customer.getFirstName(), "Akriti");

			logger.info("Customer Info: " + customer.toString());
		}
	}

	@Test
	public void updateCustomerTest() {
		List<Customer> customers = customerService.findByNameContainingIgnoreCase("Tandon");

		for (Customer customer : customers) {
			customer.setLastName("Smith");
			customer.setDateOfBirth(new GregorianCalendar(1991, 3, 11).getTime());

			customerService.update(customer);
		}

		List<Customer> customersList = customerService.findByNameContainingIgnoreCase("Smith");

		for (Customer customer : customersList) {
			assertEquals(customer.getDateOfBirth(), new GregorianCalendar(1991, 3, 11).getTime());
			assertEquals(customer.getLastName(), "Smith");
		}
	}

	@Test
	public void deleteCustomerTest() {
		List<Customer> customers = customerService.findAll();

		customerService.delete(customers);

		List<Customer> customersList = customerService.findAll();

		if (customersList.isEmpty()) {
			assertTrue("Record Deleted successfully", true);
		}
	}

}
