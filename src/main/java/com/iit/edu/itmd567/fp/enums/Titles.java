/**
 * 
 */
package com.iit.edu.itmd567.fp.enums;

/**
 * @author Admin
 *
 */
public enum Titles {
	MR("Mr."), MRS("Mrs."), MISS("Miss"), MS("Master");

	private final String title;

	private Titles(String title) {
		this.title = title;
	}

	public String getTitle() {
		return title;
	}
}
