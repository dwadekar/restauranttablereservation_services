/**
 * 
 */
package com.iit.edu.itmd567.fp.controller;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.iit.edu.itmd567.fp.exception.CustomerException;
import com.iit.edu.itmd567.fp.generator.CustomerIdGenerator;
import com.iit.edu.itmd567.fp.model.Address;
import com.iit.edu.itmd567.fp.model.Contact;
import com.iit.edu.itmd567.fp.model.Customer;
import com.iit.edu.itmd567.fp.service.CustomerService;

/**
 * @author Admin
 *
 */
@RestController
@RequestMapping("/register")
public class UserRegisterController {
	private static final Logger logger = Logger.getLogger(UserRegisterController.class);
	
	@Autowired
	private CustomerService customerService;
	
	@Autowired
	private CustomerIdGenerator customeIdGenerator;

	@PostMapping("/createUser")
	public ResponseEntity<Customer> createCustomer(@RequestBody Customer customer) {
		if (customer != null) {
			logger.info("Customer to be added:" + customer.toString());
				customer.setCustomerId(customeIdGenerator.generate());
			
			logger.info("I'm here!!");
			Address address = customer.getAddress();
			address.setCustomer(customer);
			customer.setAddress(address);

			Contact contact = customer.getContact();
			contact.setCustomer(customer);
			customer.setContact(contact);
			
			customer.setPassword(customer.getUserName());
			logger.info("Customer to be added:" + customer.getPassword());

			customerService.add(customer);
		}
		return new ResponseEntity<Customer>(customer, HttpStatus.OK);
	}
}
