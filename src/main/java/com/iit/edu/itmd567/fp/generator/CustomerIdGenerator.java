/**
 * 
 */
package com.iit.edu.itmd567.fp.generator;

import java.util.Calendar;
import java.util.Random;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;

/**
 * @author Admin
 *
 */
@Component
public class CustomerIdGenerator {

	private static final Logger logger = Logger.getLogger(CustomerIdGenerator.class);

	public String generateCustId() {
		Random random = new Random();
		int num = random.nextInt(9999);
		String code = new Integer(num).toString();

		return code;
	}

	public String generate() {
		Calendar calendar = Calendar.getInstance();
		int year = calendar.get(Calendar.YEAR);
		String customId = this.generateCustId() + new Integer(year).toString();
		logger.info("Generated Custom Id: " + customId);
		return customId;
	}
}
