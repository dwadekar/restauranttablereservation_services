/**
 * 
 */
package com.iit.edu.itmd567.fp.response;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author Admin
 *
 */
public class LocationDetails {

	@JsonProperty("entity_type")
	private String entity_type;
	@JsonProperty("entity_id")
	private Integer entity_id;
	@JsonProperty("title")
	private String title;
	@JsonProperty("latitude")
	private Double latitude;
	@JsonProperty("longitude")
	private Double longitude;
	@JsonIgnore
	private Integer city_id;
	@JsonProperty("city_name")
	private String city_name;
	@JsonIgnore
	private Integer country_id;
	@JsonProperty("country_name")
	private String country_name;

	public LocationDetails() {
	}

	public LocationDetails(String entity_type, Integer entity_id, String title, Double latitude, Double longitude,
			Integer city_id, String city_name, Integer country_id, String country_name) {
		super();
		this.entity_type = entity_type;
		this.entity_id = entity_id;
		this.title = title;
		this.latitude = latitude;
		this.longitude = longitude;
		this.city_id = city_id;
		this.city_name = city_name;
		this.country_id = country_id;
		this.country_name = country_name;
	}

	public String getEntity_type() {
		return entity_type;
	}

	public void setEntity_type(String entity_type) {
		this.entity_type = entity_type;
	}

	public Integer getEntity_id() {
		return entity_id;
	}

	public void setEntity_id(Integer entity_id) {
		this.entity_id = entity_id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Double getLatitude() {
		return latitude;
	}

	public void setLatitude(Double latitude) {
		this.latitude = latitude;
	}

	public Double getLongitude() {
		return longitude;
	}

	public void setLongitude(Double longitude) {
		this.longitude = longitude;
	}

	public Integer getCity_id() {
		return city_id;
	}

	public void setCity_id(Integer city_id) {
		this.city_id = city_id;
	}

	public String getCity_name() {
		return city_name;
	}

	public void setCity_name(String city_name) {
		this.city_name = city_name;
	}

	public Integer getCountry_id() {
		return country_id;
	}

	public void setCountry_id(Integer country_id) {
		this.country_id = country_id;
	}

	public String getCountry_name() {
		return country_name;
	}

	public void setCountry_name(String country_name) {
		this.country_name = country_name;
	}

	@Override
	public String toString() {
		return "LocationDetails [entity_type=" + entity_type + ", entity_id=" + entity_id + ", title=" + title
				+ ", latitude=" + latitude + ", longitude=" + longitude + ", city_id=" + city_id + ", city_name="
				+ city_name + ", country_id=" + country_id + ", country_name=" + country_name + "]";
	}

}
