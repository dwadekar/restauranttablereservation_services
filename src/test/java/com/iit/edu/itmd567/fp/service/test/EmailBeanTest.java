/**
 * 
 */
package com.iit.edu.itmd567.fp.service.test;

import static org.junit.Assert.assertTrue;

import org.apache.log4j.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.iit.edu.itmd567.fp.mail.EmailBean;

/**
 * @author Admin
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:com/iit/edu/itmd567/fp/config/app-context.xml")
public class EmailBeanTest {

	private static final Logger logger = Logger.getLogger(EmailBeanTest.class);
	
	@Autowired
	private EmailBean emailBean;
	
	@Test
	public void testInitPU() {
		assertTrue(true);
	}

	@Test
	public void emailSendTest() {
		logger.info("About to test send email method");
		emailBean.sendMail("dhananjay.wadekar@gmail.com", "Test Subject", "Test Body Message");
	}
}
