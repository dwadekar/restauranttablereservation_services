/**
 * 
 */
package com.iit.edu.itmd567.fp.constants;

/**
 * @author Admin
 *
 */
public class QueryParams {

	public final static String QUERY = "query";
	
	public final static String CITY_ID = "city_id";
	
	public final static String ENTITY_ID = "entity_id";
	
	public final static String ENTITY_TYPE = "entity_type";
	
	public final static String COLLECTION_ID = "collection_id";
	
	public final static String KEYWORDS = "keyword";
	
	public final static String Q = "q";
	
	public final static String RES_ID = "res_id";
}
