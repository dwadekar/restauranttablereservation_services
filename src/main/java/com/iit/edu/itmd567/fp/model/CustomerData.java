/**
 * 
 */
package com.iit.edu.itmd567.fp.model;

import java.util.List;

import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;

/**
 * @author Admin
 *
 */
@Component
@JsonPropertyOrder("customers")
public class CustomerData {

	private List<Customer> customers;

	public List<Customer> getCustomers() {
		return customers;
	}

	public void setCustomers(List<Customer> customers) {
		this.customers = customers;
	}

}
