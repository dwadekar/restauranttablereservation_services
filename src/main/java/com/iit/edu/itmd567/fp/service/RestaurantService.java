/**
 * 
 */
package com.iit.edu.itmd567.fp.service;

import java.io.IOException;

import org.apache.log4j.Logger;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.iit.edu.itmd567.fp.constants.QueryParams;
import com.iit.edu.itmd567.fp.constants.RestaurantConstants;
import com.iit.edu.itmd567.fp.response.CollectionsDetails;
import com.iit.edu.itmd567.fp.response.LocationsRequest;
import com.iit.edu.itmd567.fp.response.RestaurantsDetails;
import com.iit.edu.itmd567.fp.response.ReviewsDetails;

/**
 * @author Admin
 *
 */
@Component
public class RestaurantService {
	private static final Logger logger = Logger.getLogger(RestaurantService.class);

	public LocationsRequest searchByLocationName(String location)
			throws JsonParseException, JsonMappingException, IOException {
		String uri = RestaurantConstants.LOCATIONS_URI;
		RestTemplate restTemplate = new RestTemplate();

		UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(uri).queryParam(QueryParams.QUERY, location);

		HttpEntity<String> entity = getHttpEntity();

		HttpEntity<String> locationResponse = restTemplate.exchange(builder.build().encode().toUri(), HttpMethod.GET,
				entity, String.class);

		logger.info("Response is: " + locationResponse.getBody());

		String jsonData = locationResponse.getBody();
		LocationsRequest locationRequest = new LocationsRequest();

		locationRequest = convertJSONtoObject(jsonData, locationRequest);
		logger.info("LocationRequest is:" + locationRequest.toString());

		return locationRequest;
	}

	public RestaurantsDetails searchByKeywords(String keyword)
			throws JsonParseException, JsonMappingException, IOException {
		String uri = RestaurantConstants.SEARCH_URI;
		RestTemplate restTemplate = new RestTemplate();

		UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(uri).queryParam(QueryParams.Q, keyword);

		HttpEntity<String> entity = getHttpEntity();

		HttpEntity<String> keywordSearchResponse = restTemplate.exchange(builder.build().encode().toUri(),
				HttpMethod.GET, entity, String.class);

		logger.info("Keyword Search Response is: " + keywordSearchResponse.getBody());
		RestaurantsDetails restaurantDetails = new RestaurantsDetails();

		restaurantDetails = convertJSONtoObject(keywordSearchResponse.getBody(), restaurantDetails);
		logger.info("LocationCollections are:" + restaurantDetails.toString());

		return restaurantDetails;
	}

	public CollectionsDetails getCollectionsByCity(Integer cityId)
			throws JsonParseException, JsonMappingException, IOException {
		String uri = RestaurantConstants.COLLECTIONS_URI;
		RestTemplate restTemplate = new RestTemplate();

		UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(uri).queryParam(QueryParams.CITY_ID, cityId);

		HttpEntity<String> entity = getHttpEntity();

		HttpEntity<String> locationResponse = restTemplate.exchange(builder.build().encode().toUri(), HttpMethod.GET,
				entity, String.class);

		logger.info("Collections Response is: " + locationResponse.getBody());

		String jsonData = locationResponse.getBody();
		CollectionsDetails collectionDetails = new CollectionsDetails();

		collectionDetails = convertJSONtoObject(jsonData, collectionDetails);
		collectionDetails.setEntity_id(cityId);
		logger.info("LocationCollections are:" + collectionDetails.toString());

		return collectionDetails;
	}

	public RestaurantsDetails getRestaurantsListByCollection(Integer entityID, String entityType, Integer collectionID)
			throws JsonParseException, JsonMappingException, IOException {
		String uri = RestaurantConstants.SEARCH_URI;
		RestTemplate restTemplate = new RestTemplate();

		UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(uri).queryParam(QueryParams.ENTITY_ID, entityID)
				.queryParam(QueryParams.ENTITY_TYPE, entityType).queryParam(QueryParams.COLLECTION_ID, collectionID);

		HttpEntity<String> entity = getHttpEntity();

		HttpEntity<String> searchRestaurantResponse = restTemplate.exchange(builder.build().encode().toUri(),
				HttpMethod.GET, entity, String.class);

		logger.info("Restaurants Response is: " + searchRestaurantResponse.getBody());

		String jsonData = searchRestaurantResponse.getBody();
		RestaurantsDetails restaurantDetails = new RestaurantsDetails();

		restaurantDetails = convertJSONtoObject(jsonData, restaurantDetails);
		logger.info("LocationCollections are:" + restaurantDetails.toString());

		return restaurantDetails;
	}

	public ReviewsDetails getReviews(Integer resID) throws JsonParseException, JsonMappingException, IOException {
		String uri = RestaurantConstants.REVIEWS_URI;
		RestTemplate restTemplate = new RestTemplate();

		UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(uri).queryParam(QueryParams.RES_ID, resID);

		HttpEntity<String> entity = getHttpEntity();

		HttpEntity<String> reviewsResponse = restTemplate.exchange(builder.build().encode().toUri(), HttpMethod.GET,
				entity, String.class);

		logger.info("Reviews Response is: " + reviewsResponse.getBody());
		ReviewsDetails reviewsDetails = new ReviewsDetails();

		reviewsDetails = convertJSONtoObject(reviewsResponse.getBody(), reviewsDetails);
		logger.info("Reviews are:" + reviewsDetails.toString());

		return reviewsDetails;
	}

	@SuppressWarnings("unchecked")
	public <T> T convertJSONtoObject(String jsonData, T classRef)
			throws JsonParseException, JsonMappingException, IOException {
		ObjectMapper mapper = new ObjectMapper();
		classRef = (T) mapper.readValue(jsonData, classRef.getClass());

		return classRef;
	}

	private HttpEntity<String> getHttpEntity() {
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		headers.add("user-key", RestaurantConstants.USER_KEY);

		HttpEntity<String> entity = new HttpEntity<String>("parameters", headers);

		return entity;
	}
}
