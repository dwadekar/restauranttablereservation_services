/**
 * 
 */
package com.iit.edu.itmd567.fp.controller;

import java.io.IOException;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.iit.edu.itmd567.fp.constants.QueryParams;
import com.iit.edu.itmd567.fp.response.CollectionsDetails;
import com.iit.edu.itmd567.fp.response.LocationsRequest;
import com.iit.edu.itmd567.fp.response.RestaurantsDetails;
import com.iit.edu.itmd567.fp.response.ReviewsDetails;
import com.iit.edu.itmd567.fp.service.RestaurantService;

/**
 * @author Admin
 *
 */
@RestController
@RequestMapping("/restaurant")
@CrossOrigin
public class RestaurantController {

	private static final Logger logger = Logger.getLogger(RestaurantController.class);

	@Autowired
	RestaurantService restaurantService;

	@GetMapping("/locations")
	public CollectionsDetails getLocations(@RequestParam(QueryParams.QUERY) String location)
			throws JsonParseException, JsonMappingException, IOException {
		logger.info("Location to be searched: " + location);

		LocationsRequest locationRequest = restaurantService.searchByLocationName(location);
		CollectionsDetails collections = restaurantService
				.getCollectionsByCity(locationRequest.getLocation_suggestions().get(0).getEntity_id());
		return collections;
	}

	@GetMapping("/search")
	public RestaurantsDetails getRestaurants(@RequestParam(QueryParams.ENTITY_ID) Integer entityID,
			@RequestParam(QueryParams.ENTITY_TYPE) String entityType,
			@RequestParam(QueryParams.COLLECTION_ID) Integer collectionID)
			throws JsonParseException, JsonMappingException, IOException {
		logger.info("Search Collection by Collection ID: " + collectionID + ", Entity ID: " + entityID
				+ ", Entity Type: " + entityType);

		RestaurantsDetails restaurantDetails = restaurantService.getRestaurantsListByCollection(entityID, entityType,
				collectionID);

		return restaurantDetails;
	}

	@GetMapping("/search/{keyword}")
	public RestaurantsDetails getRestaurantsByKeyword(@PathVariable(QueryParams.KEYWORDS) String keyword)
			throws JsonParseException, JsonMappingException, IOException {
		logger.info("Keyword to be searched for: " + keyword);

		RestaurantsDetails keywordSearchResponse = restaurantService.searchByKeywords(keyword);

		return keywordSearchResponse;
	}

	@GetMapping("/reviews")
	public ReviewsDetails getReviewsForRestaurant(@RequestParam(QueryParams.RES_ID) Integer resID)
			throws JsonParseException, JsonMappingException, IOException {
		logger.info("Reviews for Restaurant ID: " + resID);

		ReviewsDetails reviewsDetails = restaurantService.getReviews(resID);

		return reviewsDetails;
	}
}
