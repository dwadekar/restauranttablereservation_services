/**
 * 
 */
package com.iit.edu.itmd567.fp.model;

import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author Admin
 *
 */
@Entity
@Table(name = "Contact_Info")
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Contact extends CommonEntity {

	private static final long serialVersionUID = 3258756088589410295L;
	@Id
	@Column(name = "contact_id", columnDefinition = "BINARY(16)")
	@GeneratedValue(generator = "gen")
	@GenericGenerator(name = "gen", strategy = "foreign", parameters = @Parameter(name = "property", value = "customer"))
	@JsonIgnore
	protected UUID id;

	@JsonProperty
	private String emailAddress;
	@JsonProperty
	private String mobileNumber;

	@JsonIgnore
	@OneToOne
	@PrimaryKeyJoinColumn
	private Customer customer;

	public Contact() {

	}

	public Contact(String emailAddress, String mobileNumber) {
		super();
		this.emailAddress = emailAddress;
		this.mobileNumber = mobileNumber;
	}

	public UUID getId() {
		return id;
	}

	public void setId(UUID id) {
		this.id = id;
	}

	public String getEmailAddress() {
		return emailAddress;
	}

	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}

	public String getMobileNumber() {
		return mobileNumber;
	}

	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	@Override
	public String toString() {
		return "Contact [id=" + id + ", emailAddress=" + emailAddress + ", mobileNumber=" + mobileNumber + "]";
	}

}
