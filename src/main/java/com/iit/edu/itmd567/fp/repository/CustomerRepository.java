/**
 * 
 */
package com.iit.edu.itmd567.fp.repository;

import java.util.List;
import java.util.UUID;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.iit.edu.itmd567.fp.model.Customer;

/**
 * @author Admin
 *
 */
@Repository
public interface CustomerRepository extends JpaRepository<Customer, UUID> {

	List<Customer> findByCustomerId(String customerId);

	@Query("select c from Customer c where c.lastName = :lastName")
	List<Customer> findByLastNameIs(@Param("lastName") String lastName);

	@Query("select c from Customer c where c.userName = :userName")
	Customer getCustomerByUsername(@Param("userName") String userName);
}
