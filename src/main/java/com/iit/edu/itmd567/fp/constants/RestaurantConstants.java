/**
 * 
 */
package com.iit.edu.itmd567.fp.constants;

/**
 * @author Admin
 *
 */
public class RestaurantConstants {

	public final static String USER_KEY = "8a5642626d5f1b8bd42e471b841a9525";
	
	public final static String LOCATIONS_URI = "https://developers.zomato.com/api/v2.1/locations";
	
	public final static String COLLECTIONS_URI = "https://developers.zomato.com/api/v2.1/collections";
	
	public final static String SEARCH_URI = "https://developers.zomato.com/api/v2.1/search";
	
	public final static String REVIEWS_URI = "https://developers.zomato.com/api/v2.1/reviews";
		
}
