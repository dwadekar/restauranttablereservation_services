/**
 * 
 */
package com.iit.edu.itmd567.fp.generator;

import java.util.Calendar;
import java.util.Random;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;

/**
 * @author Admin
 *
 */
@Component
public class ReservationIdGenerator {

	private static final Logger logger = Logger.getLogger(ReservationIdGenerator.class);
	
	public String generateReservationId() {
		Random random = new Random();
		int num = random.nextInt(9999);
		String code = new Integer(num).toString();

		return code;
	}

	public String generate() {
		Calendar calendar = Calendar.getInstance();
		int year = calendar.get(Calendar.YEAR);
		String reservId = "R" + this.generateReservationId() + new Integer(year).toString();
		logger.info("Generated Reservation Id: " + reservId);
		return reservId;
	}
}
