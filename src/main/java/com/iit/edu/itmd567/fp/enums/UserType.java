/**
 * 
 */
package com.iit.edu.itmd567.fp.enums;

/**
 * @author Admin
 *
 */
public enum UserType {
	ADMIN("Admin"), CUSTOMER("Customers");

	private final String type;

	private UserType(String type) {
		this.type = type;
	}

	public String getType() {
		return type;
	}
}
