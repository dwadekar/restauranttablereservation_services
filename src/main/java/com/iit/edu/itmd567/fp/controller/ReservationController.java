/**
 * 
 */
package com.iit.edu.itmd567.fp.controller;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.iit.edu.itmd567.fp.generator.ReservationIdGenerator;
import com.iit.edu.itmd567.fp.mail.EmailBean;
import com.iit.edu.itmd567.fp.model.Customer;
import com.iit.edu.itmd567.fp.model.Reservation;
import com.iit.edu.itmd567.fp.model.ReservationsData;
import com.iit.edu.itmd567.fp.service.CustomerService;
import com.iit.edu.itmd567.fp.service.ReservationService;

/**
 * @author Admin
 *
 */
@RestController
@RequestMapping("/reservation")
@CrossOrigin
public class ReservationController {

	private static final Logger logger = Logger.getLogger(ReservationController.class);

	@Autowired
	ReservationService reservationService;

	@Autowired
	ReservationIdGenerator reserveIdGenerator;
	
	@Autowired
	private CustomerService customerService;
	
	@Autowired
	private EmailBean emailBean;

	@Autowired
	ReservationsData reservationsData;

	@PostMapping("/book")
	public ResponseEntity<Reservation> reserveTable(@RequestBody Reservation reservation) {
		if (reservation != null) {
			logger.info("Reservation to be added:" + reservation.toString());
			reservation.setReservationId(reserveIdGenerator.generate());

			reservationService.add(reservation);
			
			Customer customer = customerService.getCustomerByUsername(reservation.getUserName());
			String message = "Your reservation ID is: " + reservation.getReservationId();
			String subject = "Reservation Details";
			emailBean.sendMail(customer.getContact().getEmailAddress(), subject, message);
		}
		return new ResponseEntity<Reservation>(reservation, HttpStatus.OK);
	}

	@GetMapping("/list")
	public List<Reservation> getReservations() {
		logger.info("Reservations Info: " + reservationService.findAll());

		reservationsData.setReservations(reservationService.findAll());
		return reservationsData.getReservations();
	}

	@GetMapping("/list/{reservationID}")
	public ResponseEntity<Reservation> getReservationDetails(@PathVariable("reservationID") String reservationID) {
		logger.info("Reservations ID to be search: " + reservationID);

		Reservation reservation = reservationService.findByReservationId(reservationID);
		return new ResponseEntity<Reservation>(reservation, HttpStatus.OK);
	}

	@GetMapping("/restaurant")
	public List<Reservation> getReservationByRestaurant(@RequestParam("restaurantID") String restaurantID) {
		logger.info("Reservations Info by Restaurant ID: " + reservationService.findByRestaurantID(restaurantID));

		reservationsData.setReservations(reservationService.findByRestaurantID(restaurantID));
		return reservationsData.getReservations();
	}

	@GetMapping("/username")
	public List<Reservation> getReservationByUsername(@RequestParam("username") String username) {
		logger.info("Reservations Info by Username: " + reservationService.findByUserName(username));

		reservationsData.setReservations(reservationService.findByUserName(username));
		return reservationsData.getReservations();
	}
	
	@PutMapping("/cancel/{reservationID}")
	public ResponseEntity<Reservation> cancelReservation(@PathVariable("reservationID") String reservationID) {
		logger.info("Reservations ID to be cancelled: " + reservationID);
		
		Reservation reservation = reservationService.cancelReservation(reservationID);
		return new ResponseEntity<Reservation>(reservation, HttpStatus.OK);
	}
}
