/**
 * 
 */
package com.iit.edu.itmd567.fp.repository;

import java.util.List;
import java.util.UUID;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.iit.edu.itmd567.fp.model.Reservation;

/**
 * @author Admin
 *
 */
@Repository
public interface ReservationRepository extends JpaRepository<Reservation, UUID> {

	List<Reservation> findByRestaurantID(String restaurantID);

	Reservation findByReservationId(String reservationId);

	List<Reservation> findByUserName(String userName);

}
