/**
 * 
 */
package com.iit.edu.itmd567.fp.exception;

/**
 * @author Admin
 *
 */
public class CustomerException extends Exception {

	private static final long serialVersionUID = -8650960207887384407L;
	private String errorMessage;

	public CustomerException(String errorMessage) {
		super(errorMessage);
		this.errorMessage = errorMessage;
	}

	public CustomerException() {
		super();
	}

	public String getErrorMessage() {
		return errorMessage;
	}
}
