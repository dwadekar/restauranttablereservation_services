/**
 * 
 */
package com.iit.edu.itmd567.fp.model;

import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author Admin
 *
 */
@Entity
@Table(name = "Address_Info")
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Address extends CommonEntity {

	private static final long serialVersionUID = 1530019298707269518L;

	@Id
	@Column(name = "address_id", columnDefinition = "BINARY(16)")
	@GeneratedValue(generator = "gen")
	@GenericGenerator(name = "gen", strategy = "foreign", parameters = @Parameter(name = "property", value = "customer"))
	@JsonIgnore
	protected UUID id;

	@JsonProperty
	private String cityName;
	@JsonProperty
	private String stateName;
	@JsonProperty
	private String countryName;
	@JsonProperty
	private String addressType;
	@JsonProperty
	private int postalCode;

	@JsonIgnore
	@OneToOne
	@PrimaryKeyJoinColumn
	private Customer customer;

	public Address() {

	}

	public Address(String cityName, String stateName, String countryName, String addressType, int postalCode) {
		super();
		this.cityName = cityName;
		this.stateName = stateName;
		this.countryName = countryName;
		this.addressType = addressType;
		this.postalCode = postalCode;
	}

	public UUID getId() {
		return id;
	}

	public void setId(UUID id) {
		this.id = id;
	}

	public String getCityName() {
		return cityName;
	}

	public void setCityName(String cityName) {
		this.cityName = cityName;
	}

	public String getStateName() {
		return stateName;
	}

	public void setStateName(String stateName) {
		this.stateName = stateName;
	}

	public String getAddressType() {
		return addressType;
	}

	public void setAddressType(String addressType) {
		this.addressType = addressType;
	}

	public int getPostalCode() {
		return postalCode;
	}

	public void setPostalCode(int postalCode) {
		this.postalCode = postalCode;
	}

	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	public String getCountryName() {
		return countryName;
	}

	public void setCountryName(String countryName) {
		this.countryName = countryName;
	}

	@Override
	public String toString() {
		return "Address [id=" + id + ", cityName=" + cityName + ", stateName=" + stateName + ", countryName="
				+ countryName + ", addressType=" + addressType + ", postalCode=" + postalCode + "]";
	}

}
