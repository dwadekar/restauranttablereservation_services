/**
 * 
 */
package com.iit.edu.itmd567.fp.model;

import java.util.List;

import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;

/**
 * @author Admin
 *
 */
@Component
@JsonPropertyOrder("reservations")
public class ReservationsData {

	private List<Reservation> reservations;

	public List<Reservation> getReservations() {
		return reservations;
	}

	public void setReservations(List<Reservation> reservations) {
		this.reservations = reservations;
	}

}
