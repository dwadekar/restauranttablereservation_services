/**
 * 
 */
package com.iit.edu.itmd567.fp.model;

import java.util.Date;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.GenericGenerator;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author Admin
 *
 */
@Entity
@Table(name = "reservation_info")
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Reservation extends CommonEntity {

	private static final long serialVersionUID = 7975262297389748163L;

	@Id
	@GeneratedValue(generator = "uuid2")
	@GenericGenerator(name = "uuid2", strategy = "uuid2")
	@Column(name = "reservation_id", columnDefinition = "BINARY(16)")
	@JsonIgnore
	protected UUID id;
	@JsonProperty
	@JsonFormat(pattern = "yyyy-MM-dd")
	@Column(name = "reservation_date", nullable = false)
	@Temporal(TemporalType.DATE)
	private Date reservationDate;
	@JsonProperty
	@Column(name = "fromTime", nullable = false)
	private String fromTime;
	@JsonProperty
	@Column(name = "toTime", nullable = false)
	private String toTime;
	@JsonProperty
	@Column(name = "restaurant_id", nullable = false)
	private String restaurantID;
	@JsonProperty
	private String status;
	@JsonProperty
	private String userName;
	@JsonProperty("reservationIdentifier")
	@Column(name = "reservation_identifier", nullable = false)
	private String reservationId;

	public Reservation() {
		super();
	}

	public Reservation(Date reservationDate, String fromTime, String toTime, String restaurantID, String status,
			String userName, String reservationId) {
		super();
		this.reservationDate = reservationDate;
		this.fromTime = fromTime;
		this.toTime = toTime;
		this.restaurantID = restaurantID;
		this.status = status;
		this.userName = userName;
		this.reservationId = reservationId;
	}

	public UUID getId() {
		return id;
	}

	public void setId(UUID id) {
		this.id = id;
	}

	public Date getReservationDate() {
		return reservationDate;
	}

	public void setReservationDate(Date reservationDate) {
		this.reservationDate = reservationDate;
	}

	public String getFromTime() {
		return fromTime;
	}

	public void setFromTime(String fromTime) {
		this.fromTime = fromTime;
	}

	public String getToTime() {
		return toTime;
	}

	public void setToTime(String toTime) {
		this.toTime = toTime;
	}

	public String getRestaurantID() {
		return restaurantID;
	}

	public void setRestaurantID(String restaurantID) {
		this.restaurantID = restaurantID;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getReservationId() {
		return reservationId;
	}

	public void setReservationId(String reservationId) {
		this.reservationId = reservationId;
	}

	@Override
	public String toString() {
		return "Reservation [id=" + id + ", reservationDate=" + reservationDate + ", fromTime=" + fromTime + ", toTime="
				+ toTime + ", restaurantID=" + restaurantID + ", status=" + status + ", userName=" + userName
				+ ", reservationId=" + reservationId + "]";
	}

}
