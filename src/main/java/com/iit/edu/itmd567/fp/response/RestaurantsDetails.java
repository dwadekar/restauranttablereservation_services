/**
 * 
 */
package com.iit.edu.itmd567.fp.response;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author Admin
 *
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class RestaurantsDetails {

	@JsonProperty("results_found")
	private Integer resultsFound;
	@JsonProperty("results_shown")
	private Integer resultShown;
	@JsonProperty("restaurants")
	private List<Restaurants> restaurants;

	public RestaurantsDetails() {
	}

	public RestaurantsDetails(Integer resultsFound, Integer resultShown, List<Restaurants> restaurants) {
		super();
		this.resultsFound = resultsFound;
		this.resultShown = resultShown;
		this.restaurants = restaurants;
	}

	public Integer getResultsFound() {
		return resultsFound;
	}

	public void setResultsFound(Integer resultsFound) {
		this.resultsFound = resultsFound;
	}

	public Integer getResultShown() {
		return resultShown;
	}

	public void setResultShown(Integer resultShown) {
		this.resultShown = resultShown;
	}

	public List<Restaurants> getRestaurants() {
		return restaurants;
	}

	public void setRestaurants(List<Restaurants> restaurants) {
		this.restaurants = restaurants;
	}

	@Override
	public String toString() {
		return "RestaurantsDetails [resultsFound=" + resultsFound + ", resultShown=" + resultShown + ", restaurants="
				+ restaurants + "]";
	}

}
