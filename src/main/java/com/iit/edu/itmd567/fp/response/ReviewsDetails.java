/**
 * 
 */
package com.iit.edu.itmd567.fp.response;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author Admin
 *
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class ReviewsDetails {

	@JsonProperty("reviews_count")
	private Integer reviewsCount;
	@JsonProperty("reviews_shown")
	private Integer reviewsShown;
	@JsonProperty("user_reviews")
	private List<UserReviews> userReviews;

	public ReviewsDetails() {
	}

	public ReviewsDetails(Integer reviewsCount, Integer reviewsShown, List<UserReviews> userReviews) {
		super();
		this.reviewsCount = reviewsCount;
		this.reviewsShown = reviewsShown;
		this.userReviews = userReviews;
	}

	public Integer getReviewsCount() {
		return reviewsCount;
	}

	public void setReviewsCount(Integer reviewsCount) {
		this.reviewsCount = reviewsCount;
	}

	public Integer getReviewsShown() {
		return reviewsShown;
	}

	public void setReviewsShown(Integer reviewsShown) {
		this.reviewsShown = reviewsShown;
	}

	public List<UserReviews> getUserReviews() {
		return userReviews;
	}

	public void setUserReviews(List<UserReviews> userReviews) {
		this.userReviews = userReviews;
	}

	@Override
	public String toString() {
		return "ReviewsDetails [reviewsCount=" + reviewsCount + ", reviewsShown=" + reviewsShown + ", userReviews="
				+ userReviews + "]";
	}

}
