/**
 * 
 */
package com.iit.edu.itmd567.fp.security;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

/**
 * @author Admin
 *
 */
public class CustomUserDetails implements UserDetails {

	private static final long serialVersionUID = -6750032598173377708L;
	private Collection<? extends GrantedAuthority> authorities;
	private String password;
	private String username;

	public CustomUserDetails(String password, String username, String userType) {
		this.password = password;
		this.username = username;
		this.authorities = translate(userType);
	}

	/**
	 * Translates the List<Role> to a List<GrantedAuthority>
	 * 
	 * @param roles
	 *            the input list of roles.
	 * @return a list of granted authorities
	 */
	private Collection<? extends GrantedAuthority> translate(String roles) {
		List<GrantedAuthority> authorities = new ArrayList<>();
		String name = roles.toUpperCase();
		// Make sure that all roles start with "ROLE_"
		if (!name.startsWith("ROLE_"))
			name = "ROLE_" + name;
		authorities.add(new SimpleGrantedAuthority(name));
		return authorities;
	}

	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		// TODO Auto-generated method stub
		return authorities;
	}

	@Override
	public String getPassword() {
		// TODO Auto-generated method stub
		return password;
	}

	@Override
	public String getUsername() {
		// TODO Auto-generated method stub
		return username;
	}

	@Override
	public boolean isAccountNonExpired() {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public boolean isAccountNonLocked() {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public boolean isCredentialsNonExpired() {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public boolean isEnabled() {
		// TODO Auto-generated method stub
		return true;
	}

}
