/**
 * 
 */
package com.iit.edu.itmd567.fp.response;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author Admin
 *
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class User {

	@JsonProperty("name")
	private String name;
	@JsonProperty("foodie_level")
	private String foodieLevel;
	@JsonProperty("foodie_level_num")
	private String foodieLevelNum;
	@JsonProperty("foodie_color")
	private String foodieColor;

	public User() {
	}

	public User(String name, String foodieLevel, String foodieLevelNum, String foodieColor) {
		super();
		this.name = name;
		this.foodieLevel = foodieLevel;
		this.foodieLevelNum = foodieLevelNum;
		this.foodieColor = foodieColor;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getFoodieLevel() {
		return foodieLevel;
	}

	public void setFoodieLevel(String foodieLevel) {
		this.foodieLevel = foodieLevel;
	}

	public String getFoodieLevelNum() {
		return foodieLevelNum;
	}

	public void setFoodieLevelNum(String foodieLevelNum) {
		this.foodieLevelNum = foodieLevelNum;
	}

	public String getFoodieColor() {
		return foodieColor;
	}

	public void setFoodieColor(String foodieColor) {
		this.foodieColor = foodieColor;
	}

	@Override
	public String toString() {
		return "User [name=" + name + ", foodieLevel=" + foodieLevel + ", foodieLevelNum=" + foodieLevelNum
				+ ", foodieColor=" + foodieColor + "]";
	}

}
