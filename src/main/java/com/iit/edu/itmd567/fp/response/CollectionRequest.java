/**
 * 
 */
package com.iit.edu.itmd567.fp.response;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author Admin
 *
 */
public class CollectionRequest {

	@JsonProperty("collection")
	private Collection collection;

	public CollectionRequest() {
	}

	public CollectionRequest(Collection collection) {
		super();
		this.collection = collection;
	}

	public Collection getCollection() {
		return collection;
	}

	public void setCollection(Collection collection) {
		this.collection = collection;
	}

	@Override
	public String toString() {
		return "CollectionRequest [collection=" + collection + "]";
	}

}
