/**
 * 
 */
package com.iit.edu.itmd567.fp.enums;

/**
 * @author Admin
 *
 */
public enum AddressType {
	HOME("Home Address"), DELIVERY("Delivery Address"), OFFICE("Office Address");

	private final String type;

	private AddressType(String type) {
		this.type = type;
	}

	public String getType() {
		return type;
	}
}
