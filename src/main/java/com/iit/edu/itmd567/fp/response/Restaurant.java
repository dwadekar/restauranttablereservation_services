/**
 * 
 */
package com.iit.edu.itmd567.fp.response;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author Admin
 *
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class Restaurant {

	@JsonProperty("id")
	private String resID;
	@JsonProperty("name")
	private String resName;
	@JsonProperty("location")
	private Location location;
	@JsonProperty("cuisines")
	private String cuisines;
	@JsonProperty("average_cost_for_two")
	private Integer averageCost;
	@JsonProperty("currency")
	private String currency;
	@JsonProperty("thumb")
	private String thumb;
	@JsonProperty("user_rating")
	private UserRating userRating;
	@JsonProperty("photos_url")
	private String photosURL;
	@JsonProperty("menu_url")
	private String menuURL;
	@JsonProperty("has_table_booking")
	private Integer hasTableBooking;

	public Restaurant() {
	}

	public Restaurant(String resID, String resName, Location location, String cuisines, Integer averageCost,
			String currency, String thumb, UserRating userRating, String photosURL, String menuURL,
			Integer hasTableBooking) {
		super();
		this.resID = resID;
		this.resName = resName;
		this.location = location;
		this.cuisines = cuisines;
		this.averageCost = averageCost;
		this.currency = currency;
		this.thumb = thumb;
		this.userRating = userRating;
		this.photosURL = photosURL;
		this.menuURL = menuURL;
		this.hasTableBooking = hasTableBooking;
	}

	public String getResID() {
		return resID;
	}

	public void setResID(String resID) {
		this.resID = resID;
	}

	public String getResName() {
		return resName;
	}

	public void setResName(String resName) {
		this.resName = resName;
	}

	public Location getLocation() {
		return location;
	}

	public void setLocation(Location location) {
		this.location = location;
	}

	public String getCuisines() {
		return cuisines;
	}

	public void setCuisines(String cuisines) {
		this.cuisines = cuisines;
	}

	public Integer getAverageCost() {
		return averageCost;
	}

	public void setAverageCost(Integer averageCost) {
		this.averageCost = averageCost;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public UserRating getUserRating() {
		return userRating;
	}

	public void setUserRating(UserRating userRating) {
		this.userRating = userRating;
	}

	public String getPhotosURL() {
		return photosURL;
	}

	public void setPhotosURL(String photosURL) {
		this.photosURL = photosURL;
	}

	public String getMenuURL() {
		return menuURL;
	}

	public void setMenuURL(String menuURL) {
		this.menuURL = menuURL;
	}

	public Integer getHasTableBooking() {
		return hasTableBooking;
	}

	public void setHasTableBooking(Integer hasTableBooking) {
		this.hasTableBooking = hasTableBooking;
	}

	public String getThumb() {
		return thumb;
	}

	public void setThumb(String thumb) {
		this.thumb = thumb;
	}

	@Override
	public String toString() {
		return "Restaurant [resID=" + resID + ", resName=" + resName + ", location=" + location + ", cuisines="
				+ cuisines + ", averageCost=" + averageCost + ", currency=" + currency + ", thumb=" + thumb
				+ ", userRating=" + userRating + ", photosURL=" + photosURL + ", menuURL=" + menuURL
				+ ", hasTableBooking=" + hasTableBooking + "]";
	}

}
