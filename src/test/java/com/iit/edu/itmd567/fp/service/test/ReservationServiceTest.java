/**
 * 
 */
package com.iit.edu.itmd567.fp.service.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.GregorianCalendar;
import java.util.List;

import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.iit.edu.itmd567.fp.enums.ReservationStatus;
import com.iit.edu.itmd567.fp.generator.ReservationIdGenerator;
import com.iit.edu.itmd567.fp.model.Reservation;
import com.iit.edu.itmd567.fp.service.ReservationService;

/**
 * @author Admin
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:com/iit/edu/itmd567/fp/config/app-context.xml")
public class ReservationServiceTest {

	private static final Logger logger = Logger.getLogger(ReservationServiceTest.class);

	@Autowired
	private ReservationService reservationService;

	@Autowired
	private ReservationIdGenerator reserveIdGenerator;
	
	private String reservationID;

	@Before
	public void setUp() {
		Reservation reservation = new Reservation(new GregorianCalendar(2017, 11, 5).getTime(), "19", "21", "1788795",
				ReservationStatus.ACTIVE.getType(), "Akriti", reserveIdGenerator.generate());

		reservationService.add(reservation);
		reservationID = reservation.getReservationId();
	}

	@After
	public void tearDown() {
		List<Reservation> reservations = reservationService.findAll();
		reservationService.delete(reservations);
	}

	/**
	 * Test Method to test the initialization of Persistence in project
	 */
	@Test
	public void testInitPU() {
		assertTrue(true);
		logger.info("Default test method executed!!");
	}

	@Test
	public void reserveTableTest() {
		Reservation reservation = new Reservation(new GregorianCalendar(2017, 11, 5).getTime(), "20", "21", "10868795",
				ReservationStatus.ACTIVE.getType(), "Akriti", reserveIdGenerator.generate());

		reservationService.add(reservation);

		assertNotNull(reservation.getId());
		assertNotNull(reservation.getReservationId());
	}

	@Test
	public void findByUsernameTest() {
		List<Reservation> reservations = reservationService.findByUserName("Akriti");

		for (Reservation reservation : reservations) {
			assertEquals(reservation.getUserName(), "Akriti");
			assertEquals(reservation.getStatus(), ReservationStatus.ACTIVE.getType());
			assertEquals(reservation.getRestaurantID(), "1788795");
		}
	}

	@Test
	public void findByRestaurantIDTest() {
		List<Reservation> reservations = reservationService.findByRestaurantID("1788795");

		for (Reservation reservation : reservations) {
			assertEquals(reservation.getUserName(), "Akriti");
			assertEquals(reservation.getStatus(), ReservationStatus.ACTIVE.getType());
			assertEquals(reservation.getRestaurantID(), "1788795");
		}
	}
	
	@Test
	public void cancelReservationTest() {
		Reservation reservation = reservationService.cancelReservation(reservationID);
		
		assertEquals(reservation.getStatus(), ReservationStatus.CANCEL.getType());
	}

}
